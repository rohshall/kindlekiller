package main

import (
"bytes"
"crypto/sha1"
"encoding/binary"
"flag"
"fmt"
"io/ioutil"
"path"
"path/filepath"
"log"
)

const headerlen int = 68
const palmdocCompression uint16 = 1
const huffmanCompression uint16 = 17480
const charMap3 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
const charMap4 = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789"
const keyVec1 = "\x72\x38\x33\xB0\xB4\xF2\xE3\xCA\xDF\x09\x01\xD6\xE2\xE0\x3F\x96"

type section struct {
	offset uint32
	flags  uint8
	val    uint32
}

func newToSection(num uint64) *section {
	return &section{
		offset: uint32(num >> 32),
		flags:  uint8(num >> 24),
		val:    uint32(num) & 0xfff,
	}
}

func pc1(key []byte, src []byte, decryption bool) []byte {
	sum1 := uint16(0)
	sum2 := uint16(0)
	keyXorVal := uint16(0)
	if len(key) != 16 {
		log.Printf("invalid key %s\n", key)
		return nil
	}
	wkey := make([]uint16, 8)
	for i := 0; i < 8; i++ {
		wkey[i] = uint16(key[i*2])<<8 | uint16(key[i*2+1])
	}
	// fmt.Printf("wkey: %v\n", wkey)
	dst := make([]byte, len(src))
	for i := 0; i < len(src); i++ {
		temp1 := uint16(0)
		byteXorVal := uint16(0)
		for j := uint16(0); j < 8; j++ {
			temp1 ^= wkey[j]
			sum2 = (sum2+j)*20021 + sum1
			sum1 = (temp1 * 346) & 0xFFFF
			sum2 = (sum2 + sum1) & 0xFFFF
			temp1 = (temp1*20021 + 1) & 0xFFFF
			byteXorVal ^= temp1 ^ sum2
		}
		curByte := src[i]
		if !decryption {
			keyXorVal = uint16(curByte) * 257
		}
		curByte = byte(((uint16(curByte) ^ (byteXorVal >> 8)) ^ byteXorVal) & 0xFF)
		if decryption {
			keyXorVal = uint16(curByte) * 257
		}
		for j := 0; j < 8; j++ {
			wkey[j] ^= keyXorVal
		}
		dst[i] = curByte
	}
	return dst
}

// Returns the six bits at offset from a bit field
func getTwoBitsFromBitField(bitfield []byte, offset uint) byte {
	byteNumber := offset / 4
	bitPosition := 6 - 2*(offset%4)
	return (bitfield[byteNumber] >> bitPosition) & 3
}

func getSizeOfTrailingDataEntry(ptr []byte, size int) int {
	bitPos, result := uint(0), 0
	if size <= 0 {
		return result
	}
	for {
		v := ptr[size-1]
		result |= int(v&0x7F) << bitPos
		bitPos += 7
		size--
		if (v&0x80) != 0 || (bitPos >= 28) || (size == 0) {
			return result
		}
	}
}

func getSizeOfTrailingDataEntries(ptr []byte, size int, flags uint16) int {
	num := 0
	testFlags := flags >> 1
	for testFlags != 0 {
		if testFlags&1 != 0 {
			num += getSizeOfTrailingDataEntry(ptr, size-num)
		}
		testFlags >>= 1
	}
	// Check the low bit to see if there's multibyte data present.
	// if multibyte data is included in the encryped data, we'll
	// have already cleared this flag.
	if flags&1 != 0 {
		num += int(ptr[size-num-1]&0x3) + 1
	}
	return num
}

func removeDrm(infile string, serialNum string) {
	dat, err := ioutil.ReadFile(infile)
	if err != nil {
		log.Println(err)
		return
	}

	magic := dat[60:68]
	if bytes.Compare([]byte("BOOKMOBI"), magic) != 0 {
		log.Printf("Ignoring %s as the format is not mobi\n", infile)
		return
	}
	numSections := binary.BigEndian.Uint16(dat[76:78])
	//fmt.Printf("%s: magic %s numSections %d\n", infile, magic, numSections)
	sections := make([]*section, numSections)
	for i := uint16(0); i < numSections; i++ {
		sectionData := binary.BigEndian.Uint64(dat[78+i*8 : 78+i*8+8])
		sections[i] = newToSection(sectionData)
		// fmt.Printf("section: %v (%X)\n", *sections[i], sectionData)
	}
	section0 := dat[sections[0].offset:sections[1].offset]
	compression := binary.BigEndian.Uint16(section0[0:0x2])
	numRecords := binary.BigEndian.Uint16(section0[0x8:0xA])
	mobiHeaderLen := binary.BigEndian.Uint32(section0[0x14:0x18])
	//mobiCodePage := binary.BigEndian.Uint32(section0[0x1C:0x20])
	mobiVersion := binary.BigEndian.Uint32(section0[0x68:0x6C])
	var extraDataFlags uint16
	if mobiHeaderLen >= 0xE4 && mobiVersion >= 5 {
		extraDataFlags = binary.BigEndian.Uint16(section0[0xF2:0xF4])
		if compression != huffmanCompression {
			// multibyte utf8 data is included in the encryption for PalmDoc compression
			// so clear that byte so that we leave it to be decrypted.
			extraDataFlags &= 0xFFFE
		}
	} else {
		extraDataFlags = 0
	}
	exthFlag := binary.BigEndian.Uint32(section0[0x80:0x84])
	// fmt.Printf("MOBI header version = %d, length = %d, numRecords = %d, compression = %d, mobiCodePage = %x, extraDataFlags = %x, exthFlag = %x\n",
	// 	mobiVersion, mobiHeaderLen, numRecords, compression, mobiCodePage, extraDataFlags, exthFlag)
	metaInfo := make(map[uint32][]byte)
	var token []byte
	var rec209 []byte
	if exthFlag&0x40 != 0 {
		//fmt.Println("EXTH record!")
		exthRecord := section0[16+mobiHeaderLen:]
		if len(exthRecord) >= 12 && bytes.Compare([]byte("EXTH"), exthRecord[0:4]) == 0 {
			nitems := binary.BigEndian.Uint32(exthRecord[8:12])
			//fmt.Printf("EXTH - num of records: %d\n", nitems)
			pos := uint32(12)
			for i := uint32(0); i < nitems; i++ {
				exthRecType := binary.BigEndian.Uint32(exthRecord[pos : pos+4])
				exthRecSize := binary.BigEndian.Uint32(exthRecord[pos+4 : pos+8])
				content := exthRecord[pos+8 : pos+exthRecSize]
				// copy the contents - because later we will patch the data
				contentCopy := make([]byte, len(content))
				copy(contentCopy, content)
				metaInfo[exthRecType] = contentCopy
				if exthRecType == 401 && exthRecSize == 9 {
					// set clipping limit to 100%
					// fmt.Printf("patching offset %d with %d\n", pos+8, 100)
					dat[sections[0].offset+16+mobiHeaderLen+pos+8] = 100
				} else if exthRecType == 404 && exthRecSize == 9 {
					// make sure text to speech is enabled
					// fmt.Printf("patching offset %d with %d\n", pos+8, 0)
					dat[sections[0].offset+16+mobiHeaderLen+pos+8] = 0
				}
				// fmt.Printf("%d %d %v\n", exthRecType, exthRecSize, content)
				pos += exthRecSize
			}
			var prs bool
			rec209, prs = metaInfo[209]
			if prs {
				// The 209 data comes in five byte groups. Interpret the last four bytes
				// of each group as a big endian unsigned integer to get a key value
				// if that key exists in the meta_array, append its contents to the token
				var buffer bytes.Buffer
				for i := 0; i < len(rec209); i += 5 {
					exthKey := binary.BigEndian.Uint32(rec209[i+1 : i+5])
					secretVal, prs := metaInfo[exthKey]
					if prs {
						//fmt.Printf("%d: %v\n", exthKey, secretVal)
						buffer.Write(secretVal)
					}
				}
				token = make([]byte, buffer.Len())
				buffer.Read(token)
			}
		}
	}
	//fmt.Printf("rec209(%d): %v\ntoken(%d): %v\n", len(rec209), rec209, len(token), token)
	var buffer bytes.Buffer
	buffer.Write([]byte(serialNum))
	buffer.Write(rec209)
	buffer.Write(token)
	// Compute book PID
	pidOriginal := make([]byte, buffer.Len())
	buffer.Read(pidOriginal)
	pidHash := sha1.Sum(pidOriginal)
	// Encode PID: 8 bits to six bits encoding from PID hash
	buffer.Reset()
	for i := uint(0); i < 24; i += 3 {
		upper := getTwoBitsFromBitField(pidHash[:], i)
		middle := getTwoBitsFromBitField(pidHash[:], i+1)
		lower := getTwoBitsFromBitField(pidHash[:], i+2)
		index := (upper << 4) + (middle << 2) + lower
		buffer.WriteByte(charMap3[index])
	}
	pidEncoded := make([]byte, buffer.Len())
	buffer.Read(pidEncoded)
	cryptoType := binary.BigEndian.Uint16(section0[0xC:0xE])
	if cryptoType == 0 {
		log.Printf("%s is not encrypted\n", infile)
		//print_replica = bytes.Compare([]byte("%MOP"), dat[sections[1].offset:sections[1].offset+4]) == 0
	} else if cryptoType != 2 && cryptoType != 1 {
		log.Printf("%s: unknown Mobipocket encryption type %d\n", infile, cryptoType)
		return
	} else {
		rec406, prs := metaInfo[406]
		if prs {
			val406 := binary.BigEndian.Uint64(rec406[0:8])
			if val406 != 0 {
				log.Printf("%s: cannot decode library or rented books\n", infile)
				return
			}
		}
		if cryptoType == 1 {
			log.Printf("%s: old Mobipocket encryption - currently not supported\n", infile)
			return
		}
		// Find the DRM keys
		drmPtr := binary.BigEndian.Uint32(section0[0xA8:0xAC])
		drmCount := binary.BigEndian.Uint32(section0[0xAC:0xB0])
		drmSize := binary.BigEndian.Uint32(section0[0xB0:0xB4])
		//drm_flags := binary.BigEndian.Uint32(section0[0xB4:0xB8])
		if drmCount == 0 {
			log.Printf("%s: Encryption not initialised. Must be opened with Mobipocket Reader first.\n", infile)
			return
		}
		drmSection := section0[drmPtr : drmPtr+drmSize]
		//fmt.Printf("DRM keys: %d %d %d %d\n", drmPtr, drmCount, drmSize, drm_flags)
		bigPid := make([]byte, 16)
		copy(bigPid, pidEncoded)
		tempKey := pc1([]byte(keyVec1), bigPid, false)
		tempKeySum := byte(0)
		for _, b := range tempKey {
			tempKeySum += b
		}
		//fmt.Printf("bigPid: %v, tempKey: %v, tempKeySum: %v\n", bigPid, tempKey, tempKeySum)
		var foundKey []byte
		for i := uint32(0); i < drmCount; i++ {
			dVerification := binary.BigEndian.Uint32(drmSection[i*0x30 : i*0x30+4])
			//			d_size := binary.BigEndian.Uint32(drmSection[i*0x30+4 : i*0x30+8])
			//			d_type := binary.BigEndian.Uint32(drmSection[i*0x30+8 : i*0x30+12])
			dCksum := drmSection[i*0x30+12]
			dCookie := drmSection[i*0x30+16 : i*0x30+48]
			if dCksum == tempKeySum {
				dCookie = pc1(tempKey, dCookie, true)
				dVer := binary.BigEndian.Uint32(dCookie[0:4])
				dFlags := binary.BigEndian.Uint32(dCookie[4:8])
				if dVerification == dVer && (dFlags&0x1F) == 1 {
					foundKey = dCookie[8:24]
					break
				}
			}
		}
		if foundKey == nil {
			log.Printf("%s: no key found!\n", infile)
			return
		}
		//fmt.Printf("%s: found key %v\n", infile, foundKey)
		// Kill the DRM keys
		for i := sections[0].offset + drmPtr; i < sections[0].offset+drmPtr+drmSize; i++ {
			dat[i] = 0
		}
		// Kill the DRM pointer
		dat[sections[0].offset+0xA8] = 0xFF
		// clear the crypto type
		dat[sections[0].offset+0xC] = 0
		dat[sections[0].offset+0xD] = 0
		// Decrypting starts
		//fmt.Println("Decrypting..")
		mobiData := make([]byte, 0)
		// Write the first sector
		mobiData = append(mobiData, dat[:sections[1].offset]...)
		// Process all records. The number of records are less than or equal to the number of sections
		for i := uint16(1); i < numRecords+1; i++ {
			var sectionData []byte
			if i == numRecords && i+1 > numSections {
				sectionData = dat[sections[i].offset:]
			} else {
				sectionData = dat[sections[i].offset:sections[i+1].offset]
			}
			extraSize := getSizeOfTrailingDataEntries(sectionData, len(sectionData), extraDataFlags)
			//fmt.Printf("record %d, extraSize %d\n", i, extraSize)
			decodedData := pc1(foundKey, sectionData[:len(sectionData)-extraSize], true)
			if i == 1 {
				//print_replica = bytes.Compare(decodedData[:4], []byte("%MOP")) == 0
			}
			mobiData = append(mobiData, decodedData...)
			if extraSize > 0 {
				mobiData = append(mobiData, sectionData[len(sectionData)-extraSize:]...)
			}
		}
		// Add the the rest of the sections after numRecords+1, if present
		if numSections > numRecords+1 {
			mobiData = append(mobiData, dat[sections[numRecords+1].offset:]...)
		}
		dir, infilename := path.Split(infile)
		outfilename := path.Base(infilename) + ".nodrm.mobi"
		err := ioutil.WriteFile(path.Join(dir, outfilename), mobiData, 0644)
		if err != nil {
			log.Println(err)
			return
		}
		fmt.Printf("Decrypted %s!\n", infile)
	}
	//fmt.Printf("cryptoType: %d, print_replica: %v\n", cryptoType, print_replica)
}

func main() {
	var (
		serialNum string
		)
	flag.StringVar(&serialNum, "s", "", "your kindle device serial number")
	flag.Parse()
	inDir := flag.Args()
	if serialNum == "" || len(inDir) == 0 {
	log.Fatalf("kindle_dedrm -s=<your kindle device serial number> <directory containing files to be decrypted>\n")
	}
	if len(serialNum) != 16 {
		log.Fatalf("invalid serial number: %s\n", serialNum)
	}
	infiles, err := ioutil.ReadDir(inDir[0])
	if err != nil {
        log.Fatal(err)
    }
	for _, infile := range infiles {
		if !infile.IsDir() {
			removeDrm(filepath.Join(inDir[0], infile.Name()), serialNum)
		}
	}
}
